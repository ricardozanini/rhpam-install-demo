Red Hat Process Automation Manager Install Demo 
===============================================
Project to automate the installation of this product localy in a standalone configuration.


Install on your machine
-----------------------
1. [Download and unzip.](https://gitlab.com/bpmworkshop/rhpam-install-demo/-/archive/master/rhpam-install-demo-master.zip)

2. Add products to installs directory, see installs/README for details and links.

3. Run 'init.sh' or 'init.bat' file. 'init.bat' must be run with Administrative privileges. 

4. Login to http://localhost:8080/business-central  (u:erics / p:redhatpam1!)

5. Other users:

  * u:kieserver      p: kieserver1!

  * u: caseuser      p: redhatpam1!

  * u: casemanager   p: redhatpam1!

  * u: casesupplier  p: redhatpam1!

6. Enjoy installed and configured Red Hat Process Automation Manager.


Optional - Install Red Hat Process Automation Manager on OpenShift Container Platform
----------------------------------------------------------------------------
See the following project to install Red Hat Process Automation Manager in a container on OpenShift
Container Platform:

- [App Dev Cloud with Red Hat Process Automation Manager Install Demo](https://gitlab.com/redhatdemocentral/rhcs-rhpam-install-demo)


Supporting Articles
-------------------
- [AppDev in Cloud - How to put Red Hat Process Automation Manager in your Cloud](http://www.schabell.org/2018/11/appdev-in-cloud-how-to-put-red-hat-process-automation-manager-in-your-cloud.html)

- [Cloud Happiness - How to install OpenShift Container Platform with new images and templates in just minutes](http://www.schabell.org/2018/11/cloud-happiness-how-to-install-openshift-container-platform-with-new-images-templates-in-minutes.html)


Released versions
-----------------
See the tagged releases for the following versions of the product:

- v1.1 - JBoss EAP 7.2 and Red Hat Process Automation Manager 7.2 installed locally.

- v1.0 - JBoss EAP 7.1.0 and Red Hat Process Automation Manager 7.1.0 installed locally.

![RHPAM Login](https://gitlab.com/bpmworkshop/rhpam-install-demo/raw/master/docs/demo-images/rhpam-login.png)

![RHPAM Business Central](https://gitlab.com/bpmworkshop/rhpam-install-demo/raw/master/docs/demo-images/rhpam-business-central.png)
